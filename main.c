#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <pcap.h>
#include <arpa/inet.h>
#include <time.h>

#ifndef __USE_BSD
#define __USE_BSD
#endif
#include <netinet/ip.h>

#ifndef __FAVOR_BSD
#define __FAVOR_BSD
#endif
#include <netinet/tcp.h>

#ifndef ARCH_IS_BIG_ENDIAN
#define ARCH_IS_BIG_ENDIAN 0
#endif
#include "md5.h"

#define VERSION "0.1.4"
#define MAX_BRUTE_PW_LEN 16

struct tcp4_pseudohdr {
	__uint32_t		saddr;
	__uint32_t		daddr;
	__uint8_t		pad;
	__uint8_t		protocol;
	__uint16_t 		len;
};

int inc_brute_pw_r(char *cur, int pos) {
    if(cur[pos] == 0) {
        cur[pos] = 33;
        return 1;
    }
    else if(cur[pos] >= 33 && cur[pos] < 126) {
        cur[pos]++;
        return 1;
    }
    else {
        cur[pos] = 33;
        if(pos < MAX_BRUTE_PW_LEN)
            return inc_brute_pw_r(cur, pos+1);
        else
            return 0;
    }
}

int inc_brute_pw(char *cur, int pos, int anum) {
    if(!anum)
        return inc_brute_pw_r(cur, pos);

    if(cur[pos] == 0) {
        cur[pos] = 48;
        return 1;
    }
    else if(cur[pos] >= 48 && cur[pos] < 57) {
        cur[pos]++;
        return 1;
    }
    else if(cur[pos] == 57) {
        cur[pos] = 65;
        return 1;
    }
    else if(cur[pos] >= 57 && cur[pos] < 90) {
        cur[pos]++;
        return 1;
    }
    else if(cur[pos] == 90) {
        cur[pos] = 97;
        return 1;
    }
    else if(cur[pos] >= 97 && cur[pos] < 122) {
        cur[pos]++;
        return 1;
    }
    else {
        cur[pos] = 48;
        if(pos < MAX_BRUTE_PW_LEN)
            return inc_brute_pw(cur, pos+1, anum);
        else
            return 0;
    }
}

md5_byte_t *calc_md5(const u_char *packet, struct pcap_pkthdr *header, const char *pw, struct ip *iphdr) {
    md5_state_t state;
    md5_byte_t *digest = (md5_byte_t *) malloc(sizeof(md5_byte_t) * 16);
    struct ip ip;
    struct tcphdr tcp;
    struct tcp4_pseudohdr phdr;

    memcpy(&ip, packet + 14, sizeof(struct ip));
    memcpy(&tcp, packet + 14 + sizeof(struct ip), sizeof(struct tcphdr));

    phdr.saddr = ip.ip_src.s_addr;
    phdr.daddr = ip.ip_dst.s_addr;
    phdr.pad = 0;
    phdr.protocol = IPPROTO_TCP;
    phdr.len = htons(header->len - 14 - sizeof(struct ip));

    md5_init(&state);

//1. the TCP pseudo-header (in the order: source IP address,
//   destination IP address, zero-padded protocol number, and
//   segment length)
    md5_append(&state, (const md5_byte_t *) &phdr, sizeof(struct tcp4_pseudohdr));

//2. the TCP header, excluding options, and assuming a checksum of
//   zero
    tcp.th_sum = 0;
    md5_append(&state, (const md5_byte_t *) &tcp, sizeof(struct tcphdr));
    
//3. the TCP segment data (if any)
    unsigned head_len = 14 + sizeof(struct ip) + (tcp.th_off << 2);
    unsigned data_len = header->len > head_len ? header->len - head_len : 0;
    md5_append(&state, (const md5_byte_t *) packet + head_len, data_len);
    
//4. an independently-specified key or password, known to both TCPs
//   and presumably connection-specific
    md5_append(&state, (const md5_byte_t *) pw, strlen(pw));
    md5_finish(&state, digest);

    if(iphdr)
        memcpy(iphdr, &ip, sizeof(struct ip));

    return digest;
}

int main(int argc, char *argv[]) {
    int verbose = 0;
    int stop = 0;
    int opt;
    char *dev = NULL;
    char *file = NULL;
    char *wordlist = NULL;
    FILE *wlist = NULL;
    int brute = 0;
    int anum = 0;
    char brute_pw[MAX_BRUTE_PW_LEN];
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle;
    const u_char *packet;
    struct pcap_pkthdr *header;
    int i, j;
    __uint8_t md5sum[16];
    __uint8_t *digest;
    char line[512];

    printf("bgp_md5crack version %s\tby Daniel Mende - dmende@ernw.de\n", VERSION);
    fflush(stdout);

    while ((opt = getopt(argc, argv, "vsbahi:f:w:")) != -1) {
        switch (opt) {
        case 'v':
            verbose = 1;
            break;
        case 's':
            stop = 1;
            break;
        case 'b':
            brute = 1;
            break;
        case 'a':
            anum = 1;
            break;
        case 'i':
            dev = optarg;
            break;
        case 'f':
            file = optarg;
            break;
        case 'w':
            wordlist = optarg;
            break;
        case 'h':
        default:
            fprintf(stderr, "Usage: %s [-v] [-s] [-a] {-w wordlist | -b} {-i iface | -f infile}\n\n", argv[0]);
            fprintf(stderr, "-v\t\t: Be verbose\n");
            fprintf(stderr, "-s\t\t: Stop after first password found\n");
            fprintf(stderr, "-a\t\t: Only bruteforce alpha-num character space\n");
            fprintf(stderr, "-w wordlist\t: Wordlist used for cracking\n");
            fprintf(stderr, "-b\t\t: Use bruteforce for cracking\n");
            fprintf(stderr, "-i iface\t: Interface to listen on for packages\n");
            fprintf(stderr, "-f infile\t: Pcap file to read packages from\n");
            return 2;
        }
    }

    if((!wordlist && !brute) || (wordlist && brute)) {
        fprintf(stderr, "Use either wordlist mode or bruteforce mode\n");
        return 2;
    }
    if(wordlist) {
        if(!(wlist = fopen(wordlist, "r"))) {
            fprintf(stderr, "Cant open wordlist: %s\n", strerror(errno));
            return 2;
        }
        if (verbose)
            printf("Using wordlist %s\n", wordlist);
    }
    if(brute) {
        bzero(brute_pw, MAX_BRUTE_PW_LEN);
        if (verbose)
            printf("Using bruteforce\n");
    }
    
    if(dev) {
        if (getuid() != 0) {
            fprintf(stderr, "You must be root for live capture\n");
            return 2;
        }
        
        if (verbose)
            printf("Using interface %s\n", dev);
        
	    handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
        if (handle == NULL) {
            fprintf(stderr, "Couldn't open device: %s\n", errbuf);
            return 2;
        }
    }
    else if(file) {
        if (verbose)
            printf("Using input file %s\n", file);
        
        handle = pcap_open_offline((const char *) file, errbuf);
        if (handle == NULL) {
            fprintf(stderr, "Couldn't open infile: %s\n", errbuf);
            return 2;
        }
    }
    else {
        fprintf(stderr, "No target given\n");
    }
    
    struct bpf_program fp;
    if (pcap_compile(handle, &fp, "dst port 179", 0, 0) == -1) {
        fprintf(stderr, "Couldn't parse filter: %s\n", pcap_geterr(handle));
        return 2;
    }
    if (pcap_setfilter(handle, &fp) == -1) {
        fprintf(stderr, "Couldn't install filter: %s\n", pcap_geterr(handle));
        return 2;
    }
    
    while(pcap_next_ex(handle, &header, &packet) > 0) {
        for (i = 14 + sizeof(struct ip) + sizeof(struct tcphdr); i < header->len; i++) {
            if (packet[i] == 19 && packet[i+1] == 18) {
                unsigned tries = 0;
                int found = 0;
                clock_t start = clock();

                if (verbose)
                    printf("Found MD5 sum \t\t'");
                for (j = 0; j < 16; j++) {
                    md5sum[j] = packet[i+2+j];
                    if (verbose)
                        printf("%.2X ", md5sum[j]);
                }
                if (verbose)
                    printf("\b'\n");

                if(wordlist) {
                    while (fgets(line, 512, wlist)) {
                        struct ip iphdr;
                        char *tmp = strchr(line, '\n');
                        if(tmp)
                            *tmp = '\0';
                        tmp = strchr(line, '\r');
                        if(tmp)
                            *tmp = '\0';
                        if (verbose)
                            printf("Trying pw %s\n", line);
                        
                        digest = calc_md5(packet, header, line, &iphdr);
                        if (verbose) {
                            printf("Calculated MD5 sum \t'");
                            for (j = 0; j < 16; j++)
                                printf("%.2X ", digest[j]);
                            printf("\b'\n");
                        }

                        if(!memcmp(md5sum, digest, 16)) {
                            clock_t end = clock();
                            printf("Found password '%s' for connection: %s -> ", line, inet_ntoa(iphdr.ip_src));
                            printf("%s\n", inet_ntoa(iphdr.ip_dst));
                            printf("after %u tries in %.2f sec\n", tries, (float) (end - start) / CLOCKS_PER_SEC);
                            found = 1;
                            if(stop)
                                return 1;
                            else
                                break;
                        }
                        
                        free(digest);
                        tries++;
                    }
                }
                else {
                    while (inc_brute_pw(brute_pw, 0, anum)) {
                        struct ip iphdr;
                        if (verbose)
                            printf("Trying pw %s\n", brute_pw);
                        
                        digest = calc_md5(packet, header, brute_pw, &iphdr);
                        if (verbose) {
                            printf("Calculated MD5 sum \t'");
                            for (j = 0; j < 16; j++)
                                printf("%.2X ", digest[j]);
                            printf("\b'\n");
                        }

                        if(!memcmp(md5sum, digest, 16)) {
                            clock_t end = clock();
                            printf("Found password '%s' for connection: %s -> ", line, inet_ntoa(iphdr.ip_src));
                            printf("%s\n", inet_ntoa(iphdr.ip_dst));
                            printf("after %u tries in %.2f sec\n", tries, (float) (end - start) / CLOCKS_PER_SEC);
                            found = 1;
                            if(stop)
                                return 1;
                            else
                                break;
                        }
                        
                        free(digest);
                        tries++;
                        if(!(tries % 1000000)) {
                            printf("Try %u current pw %s\n", tries, brute_pw);
                        }
                    }
                }
                if(!found) {
                    clock_t end = clock();
                    printf("No password found after %u tries in %.2f sec\n", tries, (float) (end - start) / CLOCKS_PER_SEC);
                }
                if(wordlist)
                    fseek(wlist, 0, SEEK_SET);
            }
        }
    }    
    
    printf("No more packages found\n");
    
    return 0;
}
