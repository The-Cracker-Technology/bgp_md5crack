bash build.sh

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Compile... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf bgp_md5crack /opt/ANDRAX/bin/bgp_md5crack 

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

chown -R andrax:andrax /opt/ANDRAX/bin/

chmod -R 755 /opt/ANDRAX/bin/
